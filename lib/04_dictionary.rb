class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
   @entries.merge!(entry) if entry.is_a? Hash
   @entries[entry] = nil if entry.is_a? String
 end

 def keywords
   @entries.keys.sort
 end

 def include?(keyword)
   return false if @entries.keys.index(keyword) == nil
   true
 end

 def find(keyword)
   result = {}
   selected = keywords.select {|ele| ele.include? keyword }
   selected.each do |sel_key|
     result = result.merge({sel_key => @entries[sel_key]})
   end
   result
 end

 def printable
   res = []
   @entries.sort.each do |key,val|
     res << "[#{key}] \"#{val}\""
   end
   res.join("\n")
 end

end
